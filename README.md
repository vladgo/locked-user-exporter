# locked_user_exporter

Collect Active Directory users info. Must be run on an Active Directory member PC.

## Install service

Copy users.ini to the current directory and run the command:

```cmd
locked-user-exporter.exe -install
```

### Remove service

```cmd
locked-user-exporter.exe -remove
```

### users.ini

Example for multiple domains:

```ini
[domain1.example.com]
user1
user2

[domain2.example.com]
user1
user2
```

### Grafana dashboard

See [example dashboard](grafana.json)

### Prometheus Alert Manager example

```yaml
- alert: UserAccountLocked
    expr: user_account_lock_status == 1
    for: 30s
    labels:
    alertname: UserAccountLocked
    annotations:
    summary: '{% raw %}User account {{ $labels.username }} in domain {{ $labels.domain }} is locked.{% endraw %}'
    description: '{% raw %}Active Directory user account {{ $labels.username }} in domain {{ $labels.domain }} is locked!{% endraw %}'
    graph_url: "{% raw %}https://grafana.example.com/grafana/d/5STAye0ik/active-directory-users?orgId=1{% endraw %}"
```
