package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/go-ini/ini"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	wapi "github.com/vadikgo/go-win64api"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/eventlog"
	"golang.org/x/sys/windows/svc/mgr"
)

type userCollector struct {
	users    []userInfo
	userDesc *prometheus.Desc
}

type userInfo struct {
	username string
	domain   string
}

const (
	serviceName = "locked_user_exporter"
)

var (
	showVersion      = flag.Bool("version", false, "Print version information.")
	isInstallService = flag.Bool("install", false, "Install Windows service.")
	isRemoveService  = flag.Bool("remove", false, "Remove Windows service.")
	buildstamp       = "current"
	githash          = "current"
	cfg              = flag.String("config", "users.ini",
		"Configuration file with users list")
	addr        = flag.String("addr", ":8088", "The address to listen on for HTTP requests.")
	config      = ini.Empty()
	queueSize   = 10 // How many query users info in parallell
	cfgFullPath = "users.ini"
)

func getMetric(username string, domain string) (float64, error) {
	locked, err := wapi.DomainUserLocked(username, domain)
	if err != nil {
		return -1, err
	}
	//log.Printf("User account %s in domain %s is locked: %t", username, domain, locked)
	if locked {
		return 1, nil
	}
	return 0, nil
}

func (c *userCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.userDesc
}

func (c *userCollector) Collect(ch chan<- prometheus.Metric) {
	for i := 0; i < len(c.users)/queueSize+1; i++ {
		start := i * queueSize
		end := start + queueSize - 1
		if end >= len(c.users) {
			end = start + len(c.users)%queueSize - 1
		}
		var wg sync.WaitGroup
		for j := start; j <= end; j++ {
			wg.Add(1)
			go func(username string, domain string) {
				defer wg.Done()
				lock, err := getMetric(username, domain)
				if err != nil {
					log.Printf("Error getting user %s in domain %s info: %v",
						username, domain, err.Error())
				} else {
					ch <- prometheus.MustNewConstMetric(
						c.userDesc,
						prometheus.GaugeValue,
						lock,
						username,
						domain,
					)
				}
			}(c.users[j].username, c.users[j].domain)
		}
		wg.Wait()
	}
}

func exePath() (string, error) {
	prog := os.Args[0]
	p, err := filepath.Abs(prog)
	if err != nil {
		return "", err
	}
	fi, err := os.Stat(p)
	if err == nil {
		if !fi.Mode().IsDir() {
			return p, nil
		}
		err = fmt.Errorf("%s is directory", p)
	}
	if filepath.Ext(p) == "" {
		p += ".exe"
		fi, err := os.Stat(p)
		if err == nil {
			if !fi.Mode().IsDir() {
				return p, nil
			}
			err = fmt.Errorf("%s is directory", p)
		}
	}
	return "", err
}

func installService(name, desc string) error {
	exepath, err := exePath()
	if err != nil {
		return err
	}
	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()
	s, err := m.OpenService(name)
	if err == nil {
		s.Close()
		return fmt.Errorf("service %s already exists", name)
	}
	s, err = m.CreateService(name, exepath, mgr.Config{DisplayName: desc, StartType: mgr.StartAutomatic}, "-config", cfgFullPath)
	if err != nil {
		return err
	}
	defer s.Close()
	err = eventlog.InstallAsEventCreate(name, eventlog.Error|eventlog.Warning|eventlog.Info)
	if err != nil {
		s.Delete()
		return fmt.Errorf("SetupEventLogSource() failed: %s", err)
	}
	return nil
}

func removeService(name string) error {
	m, err := mgr.Connect()
	if err != nil {
		return err
	}
	defer m.Disconnect()
	s, err := m.OpenService(name)
	if err != nil {
		return fmt.Errorf("service %s is not installed", name)
	}
	defer s.Close()
	err = s.Delete()
	if err != nil {
		return err
	}
	err = eventlog.Remove(name)
	if err != nil {
		return fmt.Errorf("RemoveEventLogSource() failed: %s", err)
	}
	return nil
}

type lockExporterService struct {
	stopCh chan<- bool
}

func (s *lockExporterService) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	const cmdsAccepted = svc.AcceptStop | svc.AcceptShutdown
	changes <- svc.Status{State: svc.StartPending}
	changes <- svc.Status{State: svc.Running, Accepts: cmdsAccepted}
loop:
	for {
		select {
		case c := <-r:
			switch c.Cmd {
			case svc.Interrogate:
				changes <- c.CurrentStatus
			case svc.Stop, svc.Shutdown:
				s.stopCh <- true
				break loop
			default:
				log.Printf("unexpected control request #%d", c)
			}
		}
	}
	changes <- svc.Status{State: svc.StopPending}
	return
}

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Fprintln(os.Stdout, "Git Commit Hash:", githash)
		fmt.Fprintln(os.Stdout, "Build Time:", buildstamp)
		os.Exit(0)
	}

	if *cfg == "users.ini" {
		configDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal("Error %v", err)
		}
		cfgFullPath = filepath.FromSlash(filepath.Join(configDir, *cfg))
	} else {
		cfgFullPath = *cfg
	}

	if *isInstallService {
		err := installService(serviceName, "Active Directory User Lock Prometheus Exporter")
		if err != nil {
			log.Fatal("Can't install service. Error", err)
		}
		fmt.Fprintln(os.Stdout, "Service", serviceName, "installed.")
		os.Exit(0)
	}

	if *isRemoveService {
		err := removeService(serviceName)
		if err != nil {
			log.Fatal("Can't remove service. Error", err)
		}
		fmt.Fprintln(os.Stdout, "Service", serviceName, "removed.")
		os.Exit(0)
	}

	isInteractive, err := svc.IsAnInteractiveSession()
	if err != nil {
		log.Fatal(err)
	}

	stopCh := make(chan bool)
	if !isInteractive {
		go svc.Run(serviceName, &lockExporterService{stopCh: stopCh})
	}

	config, err := ini.LoadSources(ini.LoadOptions{AllowBooleanKeys: true}, cfgFullPath)
	if err != nil {
		log.Fatalf("Error %v", err)
	}

	usersList := []userInfo{}
	for _, domainName := range config.SectionStrings() {
		var dom string
		if strings.ToLower(domainName) == "default" {
			dom = ""
		} else {
			dom = domainName
		}
		for _, userName := range config.Section(domainName).KeyStrings() {
			usersList = append(usersList, userInfo{
				username: userName,
				domain:   dom,
			})
		}
	}

	prometheus.MustRegister(&userCollector{
		users: usersList,
		userDesc: prometheus.NewDesc("user_account_lock_status", "User account lock status.",
			[]string{
				"username",
				"domain",
			}, nil),
	})

	mainExecPath, err := exePath()
	if err != nil {
		log.Fatal(err)
	}

	sl := strings.Split(*addr, ":")
	if len(sl) != 2 {
		log.Fatalf("Define address to listen in format [host]:{port}")
	}

	added, err := wapi.FirewallRuleCreate(
		"User lock exporter",
		"User account lock prometheus exporter.",
		"Prometheus Rule Group",
		mainExecPath,
		sl[1],
		wapi.NET_FW_IP_PROTOCOL_TCP,
	)
	if err != nil {
		log.Printf("Can't create firewall rule: %v", err)
	}

	if !added {
		log.Print("Firewall rule not added")
	}

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
<head><title>Active Directory user account lock status.</title></head>
<body>
<h1>Active Directory user account lock status exporter</h1>
<p><a href=/metrics>Metrics</a></p>
</body>
</html>`))
	})

	go func() {
		log.Printf("Beginning to serve on port %s", *addr)
		log.Fatal(http.ListenAndServe(*addr, nil))
	}()

	for {
		if <-stopCh {
			log.Print("Shutting down Active Directory user account lock status exporter")
			break
		}
	}
}
