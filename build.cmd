FOR /F "tokens=*" %%g IN ('date /t') do (SET STAMP=%%g)
FOR /F "tokens=*" %%g IN ('git rev-parse HEAD') do (SET HASH=%%g)
go build -ldflags="-X 'main.buildstamp=%STAMP%' -X 'main.githash=%HASH%'" -o locked_user_exporter.exe